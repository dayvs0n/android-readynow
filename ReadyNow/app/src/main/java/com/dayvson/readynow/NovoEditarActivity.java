package com.dayvson.readynow;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dayvson.readynow.models.Contato;
import com.pkmmte.view.CircularImageView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

public class NovoEditarActivity extends Activity {

    private String tipo;
    private ImageView imageView;
    private EditText nome;
    private EditText resumo;
    private EditText telefone;

    private Integer idContato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_editar);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent it = getIntent();
        if(it.hasExtra("tipo")){
            prepararState(it.getStringExtra("tipo"), it);
        }
    }

    private void prepararState(String tipo, Intent it){
        if(tipo.equals("novo")){
            getActionBar().setTitle("Novo Contato");
            CircularImageView circularImageView = (CircularImageView) findViewById(R.id.imageView);
            circularImageView.setImageResource(R.drawable.semfoto);
        }else{
            getActionBar().setTitle("Editar Contato");
            Bundle b = this.getIntent().getExtras();
            ArrayList<String> listString = b.getStringArrayList("contato");
            EditText nome = (EditText) findViewById(R.id.novo_editar_nome_contato);
            nome.setText(listString.get(0));
            EditText telefone = (EditText) findViewById(R.id.novo_editar_telefone_contato);
            telefone.setText(listString.get(1));
            EditText resumo = (EditText) findViewById(R.id.novo_editar_resumo_contato);
            resumo.setText(listString.get(2));
            CircularImageView circularImageView = (CircularImageView) findViewById(R.id.imageView);
            circularImageView.setImageResource(listString.get(3) != null && !listString.get(3).equals("") ? Integer.parseInt(listString.get(3)) : R.drawable.semfoto);
            this.idContato = Integer.parseInt(listString.get(4));
        }
        this.tipo = tipo;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_novo_editar, menu);
        return true;
    }

    private void salvarContato(){
        if(tipo.equals("novo")){
            new PostContatos().execute("http://rodoviasbrasil.com/api/rodoviasbrasil/Ocorrencias/contatos");
        }else{
            new PostContatos().execute("http://rodoviasbrasil.com/api/rodoviasbrasil/Ocorrencias/contatos/edit");
        }
    }

    private class PostContatos extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                Toast.makeText(getApplicationContext(), R.string.sucesso_salvar, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), R.string.erro_salvar, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {

            try{
                HttpClient client = new DefaultHttpClient();
                Log.i("url", params[0]);
                HttpPost post = new HttpPost(params[0]);
                Contato contato = retornarDadosView();
                Log.i("nome", contato.getNome());
                Log.i("telefone", contato.getNome());
                Log.i("resumo", contato.getNome());

                try{
                    List<NameValuePair> nameValuePairs = new ArrayList<>();
                    nameValuePairs.add(new BasicNameValuePair("id", String.valueOf(idContato)));
                    nameValuePairs.add(new BasicNameValuePair("nome", contato.getNome()));
                    nameValuePairs.add(new BasicNameValuePair("telefone", contato.getTelefone()));
                    nameValuePairs.add(new BasicNameValuePair("resumo", contato.getResumo()));

                    post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                }catch(Exception e){
                    e.printStackTrace();
                }

                HttpResponse response = client.execute(post);
                int status = response.getStatusLine().getStatusCode();
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                if(status == 200){
                    return true;
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            return false;
        }
    }

    private Contato retornarDadosView(){
        EditText nome = (EditText) findViewById(R.id.novo_editar_nome_contato);
        EditText telefone = (EditText) findViewById(R.id.novo_editar_telefone_contato);
        EditText resumo = (EditText) findViewById(R.id.novo_editar_resumo_contato);
        return new Contato(telefone.getText().toString(), nome.getText().toString(),  resumo.getText().toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.salvar){
            salvarContato();
        }
        return super.onOptionsItemSelected(item);
    }
}
