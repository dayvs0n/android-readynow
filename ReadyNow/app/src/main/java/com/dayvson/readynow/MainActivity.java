package com.dayvson.readynow;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.BoringLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.dayvson.readynow.adapters.ListContatosAdapter;
import com.dayvson.readynow.models.Contato;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private ArrayList<Contato> listaContato;
    private ListView list;
    private ListContatosAdapter contatoArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new GetContatos().execute();
    }

    private class GetContatos extends AsyncTask<String, Void, Boolean>{

        @Override
        protected void onPostExecute(Boolean result) {
            list = (ListView) findViewById(R.id.list);
            contatoArrayAdapter = new ListContatosAdapter(getApplicationContext(), 0, listaContato);

            list.setAdapter(contatoArrayAdapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView parent, View view, int position, long id) {
                    Intent it = new Intent(view.getContext(), NovoEditarActivity.class);
                    it.putExtra("tipo", "editar");
                    Bundle b = new Bundle();
                    ArrayList<String> listString = new ArrayList<String>();
                    listString.add(listaContato.get(position).getNome());
                    listString.add(listaContato.get(position).getTelefone());
                    listString.add(listaContato.get(position).getResumo());
                    listString.add(String.valueOf(listaContato.get(position).getImage()));
                    listString.add(String.valueOf(listaContato.get(position).getId()));
                    b.putStringArrayList("contato", listString);
                    it.putExtras(b);
                    startActivity(it);
                }
            });
        }

        @Override
        protected Boolean doInBackground(String... params) {
            try {
                HttpClient client = new DefaultHttpClient();
                HttpGet post = new HttpGet("http://rodoviasbrasil.com/api/rodoviasbrasil/Ocorrencias/contatos");
                HttpResponse response = client.execute(post);

                int status = response.getStatusLine().getStatusCode();
                Log.i("status", String.valueOf(status));
                if(status == 200){
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);
                    listaContato = new ArrayList<>();
                    JSONArray arr = new JSONArray(data);
                    for(int i = 0; i < arr.length(); i++){
                        if(arr.get(i) != null){
                            JSONObject object = arr.getJSONObject(i);
                            int image = 0;
                                image = R.drawable.semfoto;

                            listaContato.add(new Contato(object.getString("telefone"), object.getString("nome"), object.getString("resumo"), image, Integer.parseInt(object.getString("id"))));
                        }
                    }
                    Log.i("data", arr.get(1).toString());

                }else{
                    Toast.makeText(getApplicationContext(), R.string.erro_buscar, Toast.LENGTH_LONG).show();
                }

            }catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    private ArrayList<Contato> gerarListaContato(){
        listaContato = new ArrayList<>();

        listaContato.add(new Contato("9111", "Dayvson", "Bla bla bla", R.drawable.semfoto));
        listaContato.add(new Contato("9112", "Good", "Bla bla bla2", R.drawable.semfoto));
        listaContato.add(new Contato("9113", "Times", "Bla bla bla3", R.drawable.semfoto));

        return listaContato;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchView sv = (SearchView) menu.findItem(R.id.search_button).getActionView();
        sv.setOnQueryTextListener(new SearchFiltro());
        return true;
    }

    private class SearchFiltro implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
        @Override
        public boolean onQueryTextSubmit(String query) {
            contatoArrayAdapter.getFilter().filter(query);
            return false;
        }
        @Override
        public boolean onClose() {
            contatoArrayAdapter.getFilter().filter("");
            return false;
        }
        @Override
        public boolean onQueryTextChange(String newText) {
            contatoArrayAdapter.getFilter().filter(newText);
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.novo_contato) {
            Intent it = new Intent(this, NovoEditarActivity.class);
            it.putExtra("tipo", "novo");
            startActivity(it);
        }

        return super.onOptionsItemSelected(item);
    }
}
