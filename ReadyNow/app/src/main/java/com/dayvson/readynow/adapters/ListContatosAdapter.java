
package com.dayvson.readynow.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.dayvson.readynow.R;
import com.dayvson.readynow.RoundedImageView;
import com.dayvson.readynow.models.Contato;
import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dayvson on 01/08/2015.
 */
public class ListContatosAdapter extends ArrayAdapter<Contato> implements Filterable{

    private Context context;
    private ArrayList<Contato> contatos;
    private ArrayList<Contato> contatosFiltrados;
    private Filter filter;

    public ListContatosAdapter(Context context, int position, ArrayList<Contato> contatos){
        super(context, position, contatos);
        this.contatos = contatos;
        this.context = context;
        this.contatosFiltrados = contatos;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contato contato = contatosFiltrados.get(position);

        if(convertView == null)
            convertView = LayoutInflater.from(context).inflate(R.layout.item_list_contatos, null);

        CircularImageView circularImageView = (CircularImageView)convertView.findViewById(R.id.image_view_contato);
        circularImageView.setImageResource(contato.getImage());
        circularImageView.setBorderColor(R.color.purple);
        circularImageView.setBorderWidth(90);

        TextView nomeContato = (TextView) convertView.findViewById(R.id.text_view_nome_contato);
        nomeContato.setText(contato.getNome().toUpperCase());

        TextView telefoneContato = (TextView) convertView.findViewById(R.id.text_view_telefone_contato);
        telefoneContato.setText(contato.getTelefone());

        return convertView;
    }

    @Override
    public int getCount() {
        return contatosFiltrados.size();
    }

    @Override
    public Contato getItem(int arg0) {
        return contatosFiltrados.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return contatosFiltrados.get(arg0).getId();
    }

    public Filter getFilter() {
        Filter filter = new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence filtro) {
                FilterResults results = new FilterResults();
                //se não foi realizado nenhum filtro insere todos os itens.
                if (filtro == null || filtro.length() == 0) {
                    results.count = contatos.size();
                    results.values = contatos;
                } else {
                    //cria um array para armazenar os objetos filtrados.
                    List<Contato> itens_filtrados = new ArrayList<Contato>();

                    //percorre toda lista verificando se contem a palavra do filtro na descricao do objeto.
                    for (int i = 0; i < contatos.size(); i++) {
                        Contato data = contatos.get(i);

                        filtro = filtro.toString().toLowerCase();
                        String nome = data.getNome().toLowerCase();
                        String telefone = data.getTelefone().toLowerCase();
                        if (nome.contains(filtro) || telefone.contains(filtro)) {
                            //se conter adiciona na lista de itens filtrados.
                            itens_filtrados.add(data);
                        }
                    }
                    // Define o resultado do filtro na variavel FilterResults
                    results.count = itens_filtrados.size();
                    results.values = itens_filtrados;
                }
                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                contatosFiltrados = (ArrayList<Contato>) results.values; // Valores filtrados.
                notifyDataSetChanged();  // Notifica a lista de alteração
            }

        };
        return filter;
    }

}
