package com.dayvson.readynow.models;

/**
 * Created by Dayvson on 01/08/2015.
 */
public class Contato {

    private Integer id;
    private String telefone;
    private String nome;
    private String resumo;
    private int image;

    public Contato(String telefone, String nome, String resumo){
        this.telefone = telefone;
        this.nome = nome;
        this.resumo = resumo;
    }

    public Contato(String telefone, String nome, String resumo, int image){
        this.telefone = telefone;
        this.nome = nome;
        this.resumo = resumo;
        this.image = image;
    }

    public Contato(String telefone, String nome, String resumo, int image, Integer id){
        this.telefone = telefone;
        this.nome = nome;
        this.resumo = resumo;
        this.image = image;
        this.id = id;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String toString(){
        return nome;
    }

}
